package com.realdolmen.appointer

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.appointer.model.Doctor
import kotlinx.android.synthetic.main.activity_this_doctor.*

class ThisDoctorActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    val auth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setUpMap()
    }


    private fun setUpMap() {
        val doctorObject = intent.getParcelableExtra("doctorobject") as Doctor
        if (doctorObject.lat != null && doctorObject.lon != null) {
            val latlong = LatLng(doctorObject.lat!!, doctorObject.lon!!)
            mMap.addMarker(
                MarkerOptions()
                    .position(latlong)
                    .title(doctorObject.name)
                    .snippet(doctorObject.address)
            )
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 12f))
        }

    }

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_this_doctor)

        val doctorObject = intent.getParcelableExtra("doctorobject") as Doctor
        doctor_name.text = doctorObject.name
        doctor_address.text = doctorObject.address
        doctor_phone.text = doctorObject.phone
        doctor_mail.text = doctorObject.mail
        if(doctorObject.image != null) {
            val bitmap = decoder(doctorObject.image!!)
            photo_this_object.setImageBitmap(bitmap)
        }
        if (doctorObject.inService != true){
            doctor_service.visibility = View.VISIBLE
            btn_make_appointment.visibility = View.INVISIBLE
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btn_make_appointment.setOnClickListener{
            val doctorObject = intent.getParcelableExtra("doctorobject") as Doctor
            val intent = Intent(this, ThisDoctorCalendarActivity::class.java)
            intent.putExtra("doctorobject", doctorObject as Parcelable)
            startActivity(intent)
        }

        nav_view.menu.setGroupCheckable(0,false,true)
        nav_view.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_map ->
                    startActivity(Intent(this, MapsActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_list ->
                    startActivity(Intent(this, ListActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_agenda ->
                    startActivity(Intent(this, MyCalendarActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_account ->
                    startActivity(Intent(this, AccountActivity::class.java))
            }
            true
        }

    }

    fun decoder(base64Str: String): Bitmap {
        try {
            val imageBytes = Base64.decode(base64Str, 0)
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        } catch (e: Exception) {
            return BitmapFactory.decodeResource(this.resources, R.drawable.logo_app2)
        }
    }

}
