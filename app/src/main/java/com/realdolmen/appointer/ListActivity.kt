package com.realdolmen.appointer

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.appointer.data.ListAdapter
import com.realdolmen.appointer.model.Doctor
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    val auth = FirebaseAuth.getInstance()
    private val currentUser = auth.currentUser
    private val db = FirebaseFirestore.getInstance()
    private var adapter: ListAdapter? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    val list = ArrayList<Doctor>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)


        search.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }


        })

        val db = FirebaseFirestore.getInstance()


        db.collection("doctors").get()
            .addOnSuccessListener { task ->
                for (document in task.documents!!) {
                    list.add(document.toObject(Doctor::class.java)!!)
                    println("Succes" + document.get("name"))
                }
                layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
                adapter = ListAdapter(list!!,this)

                rcv.layoutManager = layoutManager
                rcv.adapter = adapter
                adapter!!.notifyDataSetChanged()

            }




        nav_view.selectedItemId = R.id.navigation_list
        nav_view.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_map ->
                    startActivity(Intent(this, MapsActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_list ->
                    startActivity(Intent(this, ListActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_agenda ->
                    startActivity(Intent(this, MyCalendarActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_account ->
                    startActivity(Intent(this, AccountActivity::class.java))
            }
            true
        }

    }

    fun filter(text : String){
        val filteredList: ArrayList<Doctor> = ArrayList()

        for (d in list){
            if(d.name?.toLowerCase()!!.contains(text.toLowerCase())){
                filteredList.add(d)
            }
        }
        adapter?.filterList(filteredList)
    }

}
