package com.realdolmen.appointer.data

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.realdolmen.appointer.R
import com.realdolmen.appointer.ThisDoctorActivity
import com.realdolmen.appointer.model.Doctor
import java.lang.Exception

class ListAdapter (private var list:ArrayList<Doctor>, private val context: Context) :
RecyclerView.Adapter<ListAdapter.ViewHolder>()
{

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bindItem(doctor : Doctor){
            val doctorName: TextView = itemView.findViewById(R.id.name_of_object) as TextView
            doctorName.text = doctor.name
            val doctorAddress: TextView = itemView.findViewById(R.id.location_of_object) as TextView
            doctorAddress.text = doctor.address


            itemView.setOnClickListener {
                val intent = Intent(context, ThisDoctorActivity::class.java)
                intent.putExtra("doctorobject", doctor)
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ListAdapter.ViewHolder, p1: Int) {
        p0.bindItem(list[p1])
    }

    fun filterList(filteredList : ArrayList<Doctor>){
        list = filteredList
        notifyDataSetChanged()
        println(list.size)
    }

    fun decoder(base64Str: String): Bitmap {
        try {
            val imageBytes = Base64.decode(base64Str, 0)
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        } catch (e: Exception) {
            return BitmapFactory.decodeResource(context.resources, R.drawable.logo_app2)
        }
    }
}