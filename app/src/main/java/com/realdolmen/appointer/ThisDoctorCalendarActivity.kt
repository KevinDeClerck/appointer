package com.realdolmen.appointer

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.RequiresApi
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toolbar
import com.realdolmen.appointer.model.Doctor
import kotlinx.android.synthetic.main.activity_this_doctor_calendar.*

class ThisDoctorCalendarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_this_doctor_calendar)

        webview.settings.javaScriptEnabled = true
        webview.settings.javaScriptCanOpenWindowsAutomatically = true

        val context = this

        webview.webViewClient = MyWebViewClient()

        webview.loadUrl("https://calendar.google.com/calendar?cid=cHYycXV2MjBwbDZyMnNmMnFka2VvMXVoMHNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ")
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val doctorObject = intent.getParcelableExtra("doctorobject") as Doctor
        when (item?.itemId) {
            R.id.cancel -> {
                val intent = Intent(this, ThisDoctorActivity::class.java)
                intent.putExtra("doctorobject", doctorObject as Parcelable)
                startActivity(intent)
                return true
            }
            R.id.succes -> {
                val intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_calendar, menu)
        return true
    }

    @Suppress("OverridingDeprecatedMember")
    class MyWebViewClient : WebViewClient(){

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            view?.loadUrl(url)
            return true
        }
    }


}
