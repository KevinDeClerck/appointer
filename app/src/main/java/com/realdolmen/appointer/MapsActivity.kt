package com.realdolmen.appointer

import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.maps.android.clustering.ClusterManager
import com.realdolmen.appointer.model.Doctor
import com.realdolmen.appointer.model.MyClusterItem
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val auth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        nav_view.selectedItemId = R.id.navigation_map
        nav_view.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_map ->
                    startActivity(Intent(this, MapsActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_list ->
                    startActivity(Intent(this, ListActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_agenda ->
                    startActivity(Intent(this, MyCalendarActivity::class.java))
            }
            when (item.itemId) {
                R.id.navigation_account ->
                    startActivity(Intent(this, AccountActivity::class.java))
            }
            true
        }

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setUpMap()

    }
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private var mClusterManager: ClusterManager<MyClusterItem>? = null

    private fun setUpMap(){
        mClusterManager = ClusterManager<MyClusterItem>(this, mMap)
        mMap.setOnCameraIdleListener(mClusterManager)
        mMap.setOnMarkerClickListener(mClusterManager)
        mMap.setOnInfoWindowClickListener(mClusterManager)

        addDoctors()

        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE

            )
            return
        }
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                val currentLangLong = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLangLong, 12f))
            }
        }

    }

    private  fun addDoctors(){
        db.collection("doctors").get()
            .addOnSuccessListener { task ->
                for (document in task.documents) {
                    if (document.data!!["lat"] != null && document.data!!["lon"] != null) {
                        mClusterManager!!.addItem(
                            MyClusterItem(
                                document.data!!["lat"]!! as Double,
                                document.data!!["lon"]!! as Double,
                                document.data!!["name"] as String,
                                document.data!!["address"] as String,
                                document.toObject(Doctor::class.java)!!
                            )
                        )
                    }
                }
            }
        mClusterManager!!.setOnClusterItemInfoWindowClickListener {
            val intent = Intent(this, ThisDoctorActivity::class.java)
            intent.putExtra("doctorobject", it.mTag as Parcelable)
            startActivity(intent)

        }

    }

}
