package com.realdolmen.appointer.model

import android.os.Parcel
import android.os.Parcelable

class Doctor(): Parcelable {
    var id:Long?=null
    var address:String?=null
    var name:String?=null
    var lat:Double?=null
    var lon:Double?=null
    var image:String?=null
    var phone:String?=null
    var mail:String?=null
    var inService:Boolean?=null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Long::class.java.classLoader) as? Long
        address = parcel.readString()
        name = parcel.readString()
        lat = parcel.readValue(Double::class.java.classLoader) as? Double
        lon = parcel.readValue(Double::class.java.classLoader) as? Double
        image = parcel.readString()
        phone = parcel.readString()
        mail = parcel.readString()
        inService = parcel.readValue(Boolean::class.java.classLoader) as? Boolean

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(address)
        parcel.writeString(name)
        parcel.writeValue(lat)
        parcel.writeValue(lon)
        parcel.writeString(image)
        parcel.writeString(phone)
        parcel.writeString(mail)
        parcel.writeValue(inService)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Doctor> {
        override fun createFromParcel(parcel: Parcel): Doctor {
            return Doctor(parcel)
        }

        override fun newArray(size: Int): Array<Doctor?> {
            return arrayOfNulls(size)
        }
    }
}