package com.realdolmen.appointer.model

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class MyClusterItem(lat: Double, lon: Double, title: String, address: String, tag: Doctor) : ClusterItem {
    private val mPosition: LatLng
    private val mTitle: String = title
    private val mSnipper:String = address
    val mTag: Doctor = tag

    init {
        mPosition = LatLng(lat, lon)
    }

    override fun getSnippet(): String {
        return mSnipper
    }

    override fun getTitle(): String {
        return mTitle
    }

    override fun getPosition(): LatLng {
        return mPosition
    }
}